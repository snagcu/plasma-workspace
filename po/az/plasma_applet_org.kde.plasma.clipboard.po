# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Xəyyam Qocayev <xxmn77@gmail.com>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-26 00:39+0000\n"
"PO-Revision-Date: 2021-11-02 17:10+0400\n"
"Last-Translator: Kheyyam Gojayev <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.08.2\n"

#: contents/ui/BarcodePage.qml:23
#, kde-format
msgid "QR Code"
msgstr "QR kod"

#: contents/ui/BarcodePage.qml:24
#, kde-format
msgid "Data Matrix"
msgstr "Data Matrix"

#: contents/ui/BarcodePage.qml:25
#, kde-format
msgctxt "Aztec barcode"
msgid "Aztec"
msgstr "Aztec"

#: contents/ui/BarcodePage.qml:26
#, kde-format
msgid "Code 39"
msgstr "kod 39"

#: contents/ui/BarcodePage.qml:27
#, kde-format
msgid "Code 93"
msgstr "Kod 93"

#: contents/ui/BarcodePage.qml:28
#, kde-format
msgid "Code 128"
msgstr "Kod 128"

#: contents/ui/BarcodePage.qml:44
#, kde-format
msgid "Return to Clipboard"
msgstr "Mübadilə buferinə qayıtmaq"

#: contents/ui/BarcodePage.qml:79
#, kde-format
msgid "Change the QR code type"
msgstr "QR kodu növünü dəyişmək"

#: contents/ui/BarcodePage.qml:136
#, kde-format
msgid "Creating QR code failed"
msgstr "QR kodu yardılması alınmadı"

#: contents/ui/BarcodePage.qml:148
#, kde-format
msgid ""
"There is not enough space to display the QR code. Try resizing this applet."
msgstr ""

#: contents/ui/DelegateToolButtons.qml:26
#, kde-format
msgid "Invoke action"
msgstr "Fəaliyyəti başlatmaq"

#: contents/ui/DelegateToolButtons.qml:41
#, kde-format
msgid "Show QR code"
msgstr "QR kodu göstərmək"

#: contents/ui/DelegateToolButtons.qml:57
#, kde-format
msgid "Edit contents"
msgstr "Tərkiblərinə düzəliş etmək"

#: contents/ui/DelegateToolButtons.qml:71
#, kde-format
msgid "Remove from history"
msgstr "Tarixçədən silin"

#: contents/ui/EditPage.qml:82
#, kde-format
msgctxt "@action:button"
msgid "Save"
msgstr "Saxlayın"

#: contents/ui/EditPage.qml:90
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "İmtina"

#: contents/ui/main.qml:30
#, kde-format
msgid "Clipboard Contents"
msgstr "Mübadilə buferi tərkibləri"

#: contents/ui/main.qml:31 contents/ui/Menu.qml:136
#, kde-format
msgid "Clipboard is empty"
msgstr "Mübadilə buferi boşdur"

#: contents/ui/main.qml:59
#, kde-format
msgid "Clear History"
msgstr "Tarixçəni silmək"

#: contents/ui/main.qml:71
#, kde-format
msgid "Configure Clipboard…"
msgstr "Mübadilə buferini tənzimləmək..."

#: contents/ui/Menu.qml:136
#, kde-format
msgid "No matches"
msgstr "Oxşarları yoxdur"

#: contents/ui/UrlItemDelegate.qml:107
#, kde-format
msgctxt ""
"Indicator that there are more urls in the clipboard than previews shown"
msgid "+%1"
msgstr "+%1"

#~ msgid "The QR code is too large to be displayed"
#~ msgstr "Göstərmək üçün bu QR kod çox böyükdür"

#~ msgid "Clear history"
#~ msgstr "Tarixçəni silmək"

#~ msgid "Search…"
#~ msgstr "Axtarış..."

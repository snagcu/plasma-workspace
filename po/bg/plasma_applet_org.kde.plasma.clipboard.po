# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Mincho Kondarev <mkondarev@yahoo.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-26 00:39+0000\n"
"PO-Revision-Date: 2023-05-30 21:41+0200\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.04.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: contents/ui/BarcodePage.qml:23
#, kde-format
msgid "QR Code"
msgstr "QR код"

#: contents/ui/BarcodePage.qml:24
#, kde-format
msgid "Data Matrix"
msgstr "Матрица с данни"

#: contents/ui/BarcodePage.qml:25
#, kde-format
msgctxt "Aztec barcode"
msgid "Aztec"
msgstr "Aztec"

#: contents/ui/BarcodePage.qml:26
#, kde-format
msgid "Code 39"
msgstr "Код 39"

#: contents/ui/BarcodePage.qml:27
#, kde-format
msgid "Code 93"
msgstr "Код 93"

#: contents/ui/BarcodePage.qml:28
#, kde-format
msgid "Code 128"
msgstr "Код 128"

#: contents/ui/BarcodePage.qml:44
#, kde-format
msgid "Return to Clipboard"
msgstr "Връщане в клипборда"

#: contents/ui/BarcodePage.qml:79
#, kde-format
msgid "Change the QR code type"
msgstr "Променяне типа на баркод"

#: contents/ui/BarcodePage.qml:136
#, kde-format
msgid "Creating QR code failed"
msgstr "Създаването на баркод е неуспешно"

#: contents/ui/BarcodePage.qml:148
#, kde-format
msgid ""
"There is not enough space to display the QR code. Try resizing this applet."
msgstr ""
"Няма достатъчно място за показване на QR кода. Опитайте да промените размера "
"на този аплет."

#: contents/ui/DelegateToolButtons.qml:26
#, kde-format
msgid "Invoke action"
msgstr "Извикване на действие"

#: contents/ui/DelegateToolButtons.qml:41
#, kde-format
msgid "Show QR code"
msgstr "Показване на баркод"

#: contents/ui/DelegateToolButtons.qml:57
#, kde-format
msgid "Edit contents"
msgstr "Редактиране на съдържание"

#: contents/ui/DelegateToolButtons.qml:71
#, kde-format
msgid "Remove from history"
msgstr "Премахване от историята"

#: contents/ui/EditPage.qml:82
#, kde-format
msgctxt "@action:button"
msgid "Save"
msgstr "Запазване"

#: contents/ui/EditPage.qml:90
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Отказване"

#: contents/ui/main.qml:30
#, kde-format
msgid "Clipboard Contents"
msgstr "Съдържание на клипборда"

#: contents/ui/main.qml:31 contents/ui/Menu.qml:136
#, kde-format
msgid "Clipboard is empty"
msgstr "Буферът е празен"

#: contents/ui/main.qml:59
#, kde-format
msgid "Clear History"
msgstr "Изчистване на история"

#: contents/ui/main.qml:71
#, kde-format
msgid "Configure Clipboard…"
msgstr "Конфигуриране на клипборда…"

#: contents/ui/Menu.qml:136
#, kde-format
msgid "No matches"
msgstr "<няма съвпадения>"

#: contents/ui/UrlItemDelegate.qml:107
#, kde-format
msgctxt ""
"Indicator that there are more urls in the clipboard than previews shown"
msgid "+%1"
msgstr "+%1"

# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-02 00:40+0000\n"
"PO-Revision-Date: 2022-02-19 10:09+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: package/contents/ui/BatteryItem.qml:100
#, kde-format
msgid "Charging"
msgstr "Laddas"

#: package/contents/ui/BatteryItem.qml:102
#, kde-format
msgid "Discharging"
msgstr "Laddas ur"

#: package/contents/ui/BatteryItem.qml:104 package/contents/ui/main.qml:122
#, kde-format
msgid "Fully Charged"
msgstr "Fulladdat"

#: package/contents/ui/BatteryItem.qml:106
#, kde-format
msgid "Not Charging"
msgstr "Laddas inte"

#: package/contents/ui/BatteryItem.qml:109
#, kde-format
msgctxt "Battery is currently not present in the bay"
msgid "Not present"
msgstr "Inte monterat"

#: package/contents/ui/BatteryItem.qml:121
#, kde-format
msgctxt "Placeholder is battery percentage"
msgid "%1%"
msgstr "%1 %"

#: package/contents/ui/BatteryItem.qml:185
#, kde-format
msgid ""
"This battery's health is at only %1% and it should be replaced. Contact the "
"manufacturer."
msgstr ""
"Batteriets kapacitet är bara %1 % och det behöver bytas ut. Kontakta "
"tillverkaren."

#: package/contents/ui/BatteryItem.qml:204
#, kde-format
msgid "Time To Full:"
msgstr "Tid till fulladdat:"

#: package/contents/ui/BatteryItem.qml:205
#, kde-format
msgid "Remaining Time:"
msgstr "Återstående tid:"

#: package/contents/ui/BatteryItem.qml:211
#, kde-format
msgctxt "@info"
msgid "Estimating…"
msgstr ""

#: package/contents/ui/BatteryItem.qml:221
#, kde-format
msgid "Battery Health:"
msgstr "Batterikapacitet:"

#: package/contents/ui/BatteryItem.qml:227
#, kde-format
msgctxt "Placeholder is battery health percentage"
msgid "%1%"
msgstr "%1 %"

#: package/contents/ui/BatteryItem.qml:239
#, kde-format
msgid "Battery is configured to charge up to approximately %1%."
msgstr "Batteriet är inställt att laddas upp till ungefär %1 %."

#: package/contents/ui/CompactRepresentation.qml:106
#: package/contents/ui/CompactRepresentation.qml:150
#, kde-format
msgctxt "battery percentage below battery icon"
msgid "%1%"
msgstr "%1 %"

#: package/contents/ui/main.qml:74
#, fuzzy, kde-format
#| msgid "An application is preventing sleep and screen locking:"
msgid "The battery applet has enabled suppressing sleep and screen locking"
msgstr "Ett program förhindrar viloläge och skärmlåsning:"

#: package/contents/ui/main.qml:96
#: package/contents/ui/PowerManagementItem.qml:51
#: package/contents/ui/PowerProfileItem.qml:75
#, kde-format
msgid "Power Management"
msgstr "Strömsparhantering"

#: package/contents/ui/main.qml:96
#, fuzzy, kde-format
#| msgid "Power Management"
msgid "Power and Battery"
msgstr "Strömsparhantering"

#: package/contents/ui/main.qml:116
#, kde-format
msgid "Battery is not present in the bay"
msgstr ""

#: package/contents/ui/main.qml:129
#, kde-format
msgid "Battery at %1%, not Charging"
msgstr "Batteri %1 %, laddas inte"

#: package/contents/ui/main.qml:131
#, kde-format
msgid "Battery at %1%, plugged in but still discharging"
msgstr "Batteri %1 %, inkopplat med laddas fortfarande ur"

#: package/contents/ui/main.qml:133
#, kde-format
msgid "Battery at %1%, Charging"
msgstr "Batteri %1 %, laddas"

#: package/contents/ui/main.qml:136
#, kde-format
msgid "Battery at %1%"
msgstr "Batteri %1 %"

#: package/contents/ui/main.qml:144
#, kde-format
msgid "The power supply is not powerful enough to charge the battery"
msgstr "Kraftaggregatet är inte kraftfullt nog att ladda batteriet"

#: package/contents/ui/main.qml:148
#, kde-format
msgid "No Batteries Available"
msgstr "Inga batterier tillgängliga"

#: package/contents/ui/main.qml:155
#, kde-format
msgctxt "time until fully charged - HH:MM"
msgid "%1 until fully charged"
msgstr "%1 till fulladdat"

#: package/contents/ui/main.qml:157
#, kde-format
msgctxt "remaining time left of battery usage - HH:MM"
msgid "%1 remaining"
msgstr "%1 återstår"

#: package/contents/ui/main.qml:160
#, kde-format
msgid "Not charging"
msgstr "Laddas inte"

#: package/contents/ui/main.qml:165
#, fuzzy, kde-format
#| msgid "Automatic sleep and screen locking are disabled"
msgid ""
"Automatic sleep and screen locking are disabled; middle-click to re-enable"
msgstr "Automatiskt viloläge och skärmlåsning är inaktiverade"

#: package/contents/ui/main.qml:167
#, fuzzy, kde-format
#| msgctxt "Minimize the length of this string as much as possible"
#| msgid "Manually block sleep and screen locking"
msgid "Middle-click to disable automatic sleep and screen locking"
msgstr "Förhindra manuellt viloläge och skärmlåsning"

#: package/contents/ui/main.qml:172
#, fuzzy, kde-format
#| msgid "One application has requested activating %2:"
#| msgid_plural "%1 applications have requested activating %2:"
msgid "An application has requested activating Performance mode"
msgid_plural "%1 applications have requested activating Performance mode"
msgstr[0] "Ett program har begärt att %2 aktiveras:"
msgstr[1] "%1 program har begärt att %2 aktiveras:"

#: package/contents/ui/main.qml:176
#, kde-format
msgid "System is in Performance mode; scroll to change"
msgstr ""

#: package/contents/ui/main.qml:180
#, fuzzy, kde-format
#| msgid "One application has requested activating %2:"
#| msgid_plural "%1 applications have requested activating %2:"
msgid "An application has requested activating Power Save mode"
msgid_plural "%1 applications have requested activating Power Save mode"
msgstr[0] "Ett program har begärt att %2 aktiveras:"
msgstr[1] "%1 program har begärt att %2 aktiveras:"

#: package/contents/ui/main.qml:184
#, kde-format
msgid "System is in Power Save mode; scroll to change"
msgstr ""

#: package/contents/ui/main.qml:187
#, kde-format
msgid "System is in Balanced Power mode; scroll to change"
msgstr ""

#: package/contents/ui/main.qml:299
#, kde-format
msgid "&Show Energy Information…"
msgstr "Vi&sa information om energi…"

#: package/contents/ui/main.qml:305
#, fuzzy, kde-format
#| msgid "Show Battery Percentage on Icon"
msgid "Show Battery Percentage on Icon When Not Fully Charged"
msgstr "Visa batteriets procentvärde på ikon"

#: package/contents/ui/main.qml:318
#, fuzzy, kde-format
#| msgid "&Configure Energy Saving…"
msgid "&Configure Power Management…"
msgstr "Anpassa strömspar&hantering…"

#: package/contents/ui/PowerManagementItem.qml:44
#, kde-format
msgctxt "@title:group"
msgid "Sleep and Screen Locking after Inactivity"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:89
#, kde-format
msgctxt "Sleep and Screen Locking after Inactivity"
msgid "Blocked"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:89
#, kde-format
msgctxt "Sleep and Screen Locking after Inactivity"
msgid "Automatic"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:99
#, kde-format
msgctxt ""
"@option:check Manually block sleep and screen locking after inactivity"
msgid "Manually block"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:133
#, fuzzy, kde-format
#| msgctxt "Minimize the length of this string as much as possible"
#| msgid "Manually block sleep and screen locking"
msgid "Failed to unblock automatic sleep and screen locking"
msgstr "Förhindra manuellt viloläge och skärmlåsning"

#: package/contents/ui/PowerManagementItem.qml:136
#, fuzzy, kde-format
#| msgctxt "Minimize the length of this string as much as possible"
#| msgid "Manually block sleep and screen locking"
msgid "Failed to block automatic sleep and screen locking"
msgstr "Förhindra manuellt viloläge och skärmlåsning"

#: package/contents/ui/PowerManagementItem.qml:174
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid ""
"Your laptop is configured not to sleep when closing the lid while an "
"external monitor is connected."
msgstr ""
"Den bärbara datorn är inställd att inte gå till viloläge när locket stängs "
"och en extern bildskärm är ansluten."

#: package/contents/ui/PowerManagementItem.qml:185
#, kde-format
msgid "%1 application is currently blocking sleep and screen locking:"
msgid_plural "%1 applications are currently blocking sleep and screen locking:"
msgstr[0] "%1 program förhindrar för närvarande viloläge och skärmlåsning:"
msgstr[1] "%1 program förhindrar för närvarande viloläge och skärmlåsning:"

#: package/contents/ui/PowerManagementItem.qml:205
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (%2)"
msgstr "%1 förhindrar för närvarande viloläge och skärmlåsning (%2)"

#: package/contents/ui/PowerManagementItem.qml:207
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (unknown reason)"
msgstr "%1 förhindrar för närvarande viloläge och skärmlåsning (okänd orsak)"

#: package/contents/ui/PowerManagementItem.qml:209
#, fuzzy, kde-format
#| msgid "%1 application is currently blocking sleep and screen locking:"
#| msgid_plural ""
#| "%1 applications are currently blocking sleep and screen locking:"
msgid "An application is currently blocking sleep and screen locking (%1)"
msgstr "%1 program förhindrar för närvarande viloläge och skärmlåsning:"

#: package/contents/ui/PowerManagementItem.qml:211
#, fuzzy, kde-format
#| msgid "%1 is currently blocking sleep and screen locking (unknown reason)"
msgid ""
"An application is currently blocking sleep and screen locking (unknown "
"reason)"
msgstr "%1 förhindrar för närvarande viloläge och skärmlåsning (okänd orsak)"

#: package/contents/ui/PowerManagementItem.qml:215
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerManagementItem.qml:217
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: unknown reason"
msgstr "%1: okänd orsak"

#: package/contents/ui/PowerManagementItem.qml:219
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: %1"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:221
#, fuzzy, kde-format
#| msgctxt "Application name: reason for preventing sleep and screen locking"
#| msgid "%1: unknown reason"
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: unknown reason"
msgstr "%1: okänd orsak"

#: package/contents/ui/PowerProfileItem.qml:39
#, kde-format
msgid "Power Save"
msgstr "Spara ström"

#: package/contents/ui/PowerProfileItem.qml:43
#, kde-format
msgid "Balanced"
msgstr "balanserad"

#: package/contents/ui/PowerProfileItem.qml:47
#, kde-format
msgid "Performance"
msgstr "Prestanda"

#: package/contents/ui/PowerProfileItem.qml:64
#, kde-format
msgid "Power Profile"
msgstr "Strömsparprofil"

#: package/contents/ui/PowerProfileItem.qml:107
#, fuzzy, kde-format
#| msgid "No Batteries Available"
msgctxt "Power profile"
msgid "Not available"
msgstr "Inga batterier tillgängliga"

#: package/contents/ui/PowerProfileItem.qml:143
#, kde-format
msgid "Failed to activate %1 mode"
msgstr "Misslyckades aktivera %1"

#: package/contents/ui/PowerProfileItem.qml:220
#, kde-format
msgid ""
"Performance mode has been disabled to reduce heat generation because the "
"computer has detected that it may be sitting on your lap."
msgstr ""
"Prestandaläge har inaktiverats för att reducera värmeproduktion eftersom "
"datorn har detekterat att du har den i knäet."

#: package/contents/ui/PowerProfileItem.qml:222
#, kde-format
msgid ""
"Performance mode is unavailable because the computer is running too hot."
msgstr "Prestandaläge är inte tillgängligt eftersom datorn är för varm."

#: package/contents/ui/PowerProfileItem.qml:224
#, kde-format
msgid "Performance mode is unavailable."
msgstr "Prestandaläge är inte tillgängligt."

#: package/contents/ui/PowerProfileItem.qml:237
#, kde-format
msgid ""
"Performance may be lowered to reduce heat generation because the computer "
"has detected that it may be sitting on your lap."
msgstr ""
"Prestanda kan ha minskats för att reducera värmeproduktion eftersom datorn "
"har detekterat att du har den i knäet."

#: package/contents/ui/PowerProfileItem.qml:239
#, kde-format
msgid "Performance may be reduced because the computer is running too hot."
msgstr "Prestanda kan vara reducerad eftersom datorn är för varm."

#: package/contents/ui/PowerProfileItem.qml:241
#, kde-format
msgid "Performance may be reduced."
msgstr "Prestanda kan vara reducerad."

#: package/contents/ui/PowerProfileItem.qml:252
#, kde-format
msgid "One application has requested activating %2:"
msgid_plural "%1 applications have requested activating %2:"
msgstr[0] "Ett program har begärt att %2 aktiveras:"
msgstr[1] "%1 program har begärt att %2 aktiveras:"

#: package/contents/ui/PowerProfileItem.qml:270
#, kde-format
msgctxt ""
"%1 is the name of the application, %2 is the reason provided by it for "
"activating performance mode"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerProfileItem.qml:289
#, kde-kuit-format
msgid ""
"Power profiles may be supported on your device.<nl/>Try installing the "
"<command>power-profiles-daemon</command> package using your distribution's "
"package manager and restarting the system."
msgstr ""

#: plugin/batteriesnamesmonitor_p.cpp:25
#, fuzzy, kde-format
#| msgid "Battery at %1%"
msgctxt "Placeholder is the battery number"
msgid "Battery %1"
msgstr "Batteri %1 %"

#: plugin/batteriesnamesmonitor_p.cpp:27
#, kde-format
msgid "Battery"
msgstr "Batteri"

#~ msgid "The battery applet has enabled system-wide inhibition"
#~ msgstr "Batteriminiprogrammet har aktiverat systemövergripande inhibering"

#~ msgctxt "Minimize the length of this string as much as possible"
#~ msgid "Manually block sleep and screen locking"
#~ msgstr "Förhindra manuellt viloläge och skärmlåsning"

#~ msgctxt "Placeholder is brightness percentage"
#~ msgid "%1%"
#~ msgstr "%1 %"

#~ msgid "Battery and Brightness"
#~ msgstr "Batteri och ljusstyrka"

#~ msgid "Brightness"
#~ msgstr "Ljusstyrka"

#~ msgid "Display Brightness"
#~ msgstr "Bildskärmens ljusstyrka"

#~ msgid "Keyboard Brightness"
#~ msgstr "Tangentbordets ljusstyrka"

#, fuzzy
#~| msgid "Performance mode is unavailable."
#~ msgid "Performance mode has been manually enabled"
#~ msgstr "Prestandaläge är inte tillgängligt."

#~ msgid ""
#~ "Performance mode is unavailable because the computer has detected it is "
#~ "sitting on your lap."
#~ msgstr ""
#~ "Prestandaläge är inte tillgängligt eftersom datorn har detekterat att du "
#~ "har den i knäet."

#~ msgid "General"
#~ msgstr "Allmänt"

#~ msgctxt "short symbol to signal there is no battery currently available"
#~ msgid "-"
#~ msgstr "-"

#~ msgid "Configure Power Saving..."
#~ msgstr "Anpassa strömsparhantering..."

#~ msgid "Time To Empty:"
#~ msgstr "Tid till tomt:"

#~ msgid "Power management is disabled"
#~ msgstr "Strömsparhantering är inaktiverad"

#~ msgid ""
#~ "Disabling power management will prevent your screen and computer from "
#~ "turning off automatically.\n"
#~ "\n"
#~ "Most applications will automatically suppress power management when they "
#~ "don't want to have you interrupted."
#~ msgstr ""
#~ "Inaktivering av strömsparhantering förhindrar att bildskärmen och datorn "
#~ "stängs av automatiskt.\n"
#~ "\n"
#~ "De flesta program förhindrar automatiskt strömsparhantering när de inte "
#~ "vill att du ska bli avbruten."

#~ msgctxt "Some Application and n others are currently suppressing PM"
#~ msgid ""
#~ "%2 and %1 other application are currently suppressing power management."
#~ msgid_plural ""
#~ "%2 and %1 other applications are currently suppressing power management."
#~ msgstr[0] ""
#~ "%2 och %1 annat program förhindrar för närvarande strömsparhantering."
#~ msgstr[1] ""
#~ "%2 och %1 andra program förhindrar för närvarande strömsparhantering."

#~ msgctxt "Some Application is suppressing PM"
#~ msgid "%1 is currently suppressing power management."
#~ msgstr "%1 förhindrar för närvarande strömsparhantering."

#~ msgctxt "Some Application is suppressing PM: Reason provided by the app"
#~ msgid "%1 is currently suppressing power management: %2"
#~ msgstr "%1 förhindrar för närvarande strömsparhantering: %2"

#~ msgctxt "Used for measurement"
#~ msgid "100%"
#~ msgstr "100 %"

#~ msgid "%1% Charging"
#~ msgstr "%1 % laddas"

#~ msgid "%1% Plugged in"
#~ msgstr "%1 % inkopplad"

#~ msgid "%1% Battery Remaining"
#~ msgstr "%1 % batteri återstår"

#~ msgctxt "The degradation in the battery's energy capacity"
#~ msgid "Capacity degradation:"
#~ msgstr "Kapacitetsförsämring:"

#~ msgctxt "Placeholder is battery's capacity degradation"
#~ msgid "%1%"
#~ msgstr "%1 %"

#~ msgctxt "Placeholder is battery capacity"
#~ msgid "%1%"
#~ msgstr "%1 %"

#~ msgid "Vendor:"
#~ msgstr "Tillverkare:"

#~ msgid "Model:"
#~ msgstr "Modell:"

#~ msgid "No screen or keyboard brightness controls available"
#~ msgstr ""
#~ "Inga kontroller för skärmens eller tangentbordets ljusstyrka tillgängliga"

#~ msgctxt "Placeholders are current and maximum brightness step"
#~ msgid "%1/%2"
#~ msgstr "%1/%2"

#~ msgctxt "Placeholder is battery name"
#~ msgid "%1:"
#~ msgstr "%1:"

#~ msgid "N/A"
#~ msgstr "Ej tillgänglig"
